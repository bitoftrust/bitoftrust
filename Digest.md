# Why _bitoftrust_?

We believe in the power of trust as the foundation of an Open Society supported by an Open Web, a Web of Trust (WoT). Unfortunately, the current Web is anything but a Web of Trust, this despite the many efforts deployed to improve it (public/private key infrastructure, Verifiable Claims, DIDs, blockchains, etc.). We believe that the failure to building a WoT is to be found in the focus placed primarily on technologies instead of fostering the social aspect of trust which is tightly connected to humans.  We have been trying to design a WoT _for_ the people rather than _with_ them, building a ready-made WoT, rather than the instruments for the people to co-construct the Web of Trust.

_Bitoftrust_ is the name of the object and the project dedicated to creating the conditions for co-constructing a Web of Trust, the condition for the emergence of a new type of applications and services, _trust-based_.

While previous attempts at building a WoT was focused on _privacy_, a construct that has more to do with _distrust_ that _trust_, _bitoftrust_ is focused on _intimacy_, a construct _intimately_ related to _trust_.

# What should _bitoftrust_ achieve?
_Bitoftrusts_ allow the construction of an _intimacy by design_ Web of Trust.

By creating the conditions for the emergence of an effective Web of Trust, _bitoftrusts_ will contribute to:
* The emergence of new applications and services, trust-based;
* Empowering individuals and communities to shape the Web we want;
* Improving our control on our data and how it is being used;
* Improving our ability to use the Web as active citizens;
* Challenging LinkedIn, Facebook and other “social networks”;
* Challenging Google My Business, Tripadvisor and other reputation systems;
* Combating trolls, fake news and other Web nuisances;
* ...

# What is a _bitoftrust_?
A Web of Trust is a complex network of trust connections. A _bitoftrust_ is the smallest possible WoT, a trust connection between two entities. Using  _bitoftrust_ as the elementary construct of a Web of Trust, individuals (and other entities like organisations, communities, networks, etc.) have the power to be the active co-constructors and curators of the Web of Trust.

A _bitoftrust_ is:
* A trust token defining...
* A space in the Web of Trust constructed via...
* A protocol for distributed trust management.

A _bitoftrust_ token [E1, D, E2] is composed of three identifiers:
* __E1__, __E2__: the identifiers of the entities that are in a trust relationship (pseudonyms):
* __D__: the identifier of the _domain_ (or context) where this trust relationship is taking place (hashtags).

![web of trust](img/BWoT.png)

To be _activated_, a bitoftrust is _pushed_ to the Web of Trust by the two entities in the trust relationship. The hash of the _bitoftrust_ is used to define an address for the storage of its content. A _bitoftrust_ is revoked when one of the parties decides to erase the content of that address.

The creation, activation, update and revocation of a _bitoftrust_ is performed by a git-like application, _Gitoftrust_, supporting a local consensus for distributed trust management.


# What is a _bitoftrust-based Web of Trust_ made of?
A bitoftrust-based Web of Trust (WoT) is composed of of different components that individuals can use to co-construct a Web of Trust—by adding more _bitoftrusts_.

| _What_ | _What for_ | _Example_|
|------|-------|-------|
| _Bitoftrust_ |The elementary building blocks of the Web of Trust representing a trust relationship between two entities. | In the Web of documents, a bitoftrust would be a URL or an hypertext link to a Web page. |
| _Portfolio_ |The private offline individual space used to create, store, organise and activate (push) bitoftrusts. | Think of an address book where each entry matches an entry in other address books.|
| _Pod_ | The online space where the contents of bitoftrusts are stored when activated (pushed). Only the owners of the bitoftrusts can see their intimate contents.| Think of a Google Drive directory where you could share files and messages with one person you trust.|
| _Domain_ | The collection of Pods relative to certain domain (or context). Pods restricted data is visible by all the members of the domain. | Think of a collection of Google Drive directories where certain files are shared with all the participants in the collection.|
| _Web of Trust_ | The collection of all the domains. Pods public data is visible to all the members of all the domains. | Think of a collection of all the collections Google Drive directories where certain files are shared between everybody.|

The creation, activation, update and revocation of _bitoftrusts_ are performed by a git-like application: _Gitoftrust_ which is part of the _Portfolio of Trust_.

# What are the characteristics of a _BWoT_?
Among other things, a BWoT is:
* __People-centred__: bitoftrusts are simple constructs that individuals can activate to building their own Web of Trust and in doing so contributing to the global WoT;
* __Liable__: bad behaviour, as defined by a community, could lead to loosing trust and WoT membership;
* __Safe__: joining the WoT is through co-optation, making it difficult for trolls to join and operate freely;
* __Organic__: the Web of Trust is built from trust relationships. An entity can exist autonomously if it is in a trust relationship with at least four autonomous entities
* __Continuous__: different collections of bitoftrusts define their own spaces of intimacy—like matriochka dolls;
* __Ordered__: each _bitoftrust_ exists within a domain;
* __Dynamic__: based on the rythm and content of _pushes_, _bitoftrust_ can grow and decay.
* ...

Notes:

The collection of _bitoftrusts_ B1\*B2...\*Bn has its own space of intimacy (for the holders of those _bitoftrusts_) that is different from B1\*B2...\*Bn\*Bn+1. To create a new space of intimacy, it is just necessary to combine a collection of bitoftrusts. No need to manage unwieldy access control lists (ACL).

_Ibid._, the revocation of a _bitoftrust_ is simply done by erasing the address where it is stored in the WoT. No need for revocation lists.

A special kind of bitoftrust is the _Subject__, i.e. a bitoftrust where the two identifiers refer to the same entity [Ei, Dk, Ei]. In order to create the Subject [Ei, Dk, Ei], the entity Ei needs to be in a relationship with four other entities that are themselves Subjects in the domain Dk. This is a way to make the WoT safe, by making it difficult for trolls to join and operate freely: the people who would have allowed a troll to join might loose their trust connections.

# Use cases
## Troll free Wikipedia
In [_Facebook, Axios And NBC Paid This Guy To Whitewash Wikipedia Pages_](https://www.huffpost.com/entry/wikipedia-paid-editing-pr-facebook-nbc-axios_n_5c63321be4b03de942967225), the author explains how Ed Sussman, a PR professional, while apparently playing by the book, has 100% success in achieving desired edits to improve his clients’ image. This is one of many examples where untrustworthy people can successfully tweak Wikipedia articles.

_Bitoftrusts_ are used to create trust networks related to _domains_. One of the requirements to become and editor of the Wikipedia page related to a specific _domain_ should be to be a recognised member of the _domain_ in the WoT. When someone applies to become editor/moderator of that page, Wikipedia could ask members of the _domain_'s network to vet/confirm the candidate.

For a troll like Ed Sussman to be able bully editors (in order to get paid by his clients) he first has to seek the recognition by either members of the _domain_ or _Wikipedia_. Let's say that he succeeds to exist as a _Subject_ in the _domain_ or _Wikipedia_. After the first attempt at bullying the editors, they could inform the people who have established a trust relationship with Sussman.

## Solid Trust Pods
[Solid](https://solid.mit.edu/) defines Pods as _personal online data stores_, i.e. a kind of digital safe used to enforce _privacy_. If we change the function of a Solid Pod from storing personal data to storing _bitoftrusts_, we would have the means establish a BWoT.

The contents of Pods is managed by their owners through their local portfolios of trust and the Gitoftrust client application.

The hash of a _bitoftrust_ is used as the address of a Pod—shared by the two entities in the relationship. The hash of a collection of _Bitoftrusts_ (B1\*B2\*...\*Bn) defines the space for the owners of the _Bitoftrusts_. No need for Access Control Lists: it is the combination of _bitoftrusts_ that defines the access rights to the data.

If an entity has a Facebook account, a _bitoftrust_ is created between the two entities in the context of Facebook [E1, Facebook, Facebook]. It is this _bitofotrust_ which is used to store the data created within Facebook. Entities can create _bitoftrusts_ in the context of Facebook [E1, Facebook, E2] and use that space to store information they want to share in that context in all autonomy. Facebook could use the properties of _bitoftrust_ aggregation to facilitate data sharing.

NB: The function of _personal online data store_ is fulfilled for _Subjects_ within a specific context [Ei, Dk, Ei] or a global one [Ei, Null, Ei], i.e. domain free. All the other Pods are shared Trust Pods.










