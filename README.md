# BitofTrust

> If Blockchain was about trust-minimisation, BitofTrust is about trust-maximisation

Welcome to BitofTrust!
BitofTrust is a new way of looking at the Web of Trust, starting from concepts & constructs that
are consistent with the meaning and structure of trust:

- **Intimacy** as a more potent and relevant starting point than *privacy*
- **Trust relationships** as more relevant persistent identifiers than *personal identifiers*

> Privacy & Self-Sovereign identity has more to do with distrust than trust

BitofTrust is:

- a **token** which holds a trust relationship
- a **content address**, a type of Uniform Resource Locator (URL)
- a **Git-like protocol** to achieve consensus

## Table of Contents
- [Some History & Context](#some-history-context)
- [What problem does BitofTrust solve?](#what-problem-does-bitoftrust-solve)
- [BitofTrust and the current Web](#bitoftrust-and-the-current-web)
- [BitofTrust and Verifiable Credentials](#bitoftrust-and-verifiable-credentials)
- [BitofTrust and Solid](#bitoftrust-and-solid)
- [BitofTrust and Open Badges](#bitoftrust-and-open-badges)


## Some History & Context

This document is a work in progress. 

The concept of BitofTrust originates from the Open Badges Community and was a first time proposed
by **Serge Ravet** in a 2015 blogpost: [#Openbadges + #Blockchains = #BitofTrust?](http://www.learningfutures.eu/2015/11/openbadges-blockchains-bittrust/)

> The idea originates from the realisation, as early as 2012, that if Open Badges are trust relationships (between the issuer and recipient) then why did Mozilla create a technology, the Backpack,
that denied individuals the right to issue Open Badges / trust statements?

It took some time to realise that what once was seen as a **'trustless' technology, blockchain, actually
is a technology thriving on distrust**. Late 2018, early 2019 the insights came that if we really want to establish
a Web of Trust the **privacy perspective starting point**, the blueprint from [PGP](https://en.wikipedia.org/wiki/Web_of_trust) & later the [Rebooting the Web of Trust](https://www.weboftrust.info/),
is **wrong**.

This document is **a collective community effort to rethink the Web of Trust**, starting from concepts as **intimacy
& persistent trust relationships** and **not** from **privacy & self**.

BitofTrust is a part of the [Open Recognition movement (#openrecognition)](http://openrecognition.org/) & has been worked on & discussed by the following people (among others, our appologies if you have contributed in the past and we forgot to add your name, feel free to add yourself
or let us know so we can add you):

*Serge Ravet, Nate Otto, Esther Linley, Bert Jehoul, Kerri Lemoie, Agis Papantoniou, Julie Keane,
Don Presant, Roxanne Bhoori, Patrice Petitqueux, Kim Hamilton-Duffy, Grégoire d'Oultrement, Victoria Fontan,
Eric Rouselle, Caroline Belan-Menagier, Simone Ravaioli, Valentina Staveris, Didier Moreau, Philippe Petitqueux,
Nathan Tufte, Michiel Leyman, Justin Mason, Dries Vanacker, ...* 



This document is an ever changing snapshot of current reflections of the collective reasoning of all. Feel free to comment, add or suggest changes.


## What problem does BitofTrust solve?

Verification of digital information in the current web is based on the trust that people have in the carriers of that information
(the servers the data is stored on, the DNS providers that point you to the information, the authentication protocols used by those servers).

For a regular user all this trust is only implicit present when surfing the web.
The only trust that is really explicit for the user is what is human readable visible on the pages or applications that are visited
(the domain name that is visible in the browsers & other indicators that are visible on a page or in an application
(e.g. for a profile: name, picture, network connections, consistent data with what you previously know, ...)).

You consider digital information as true because you trust the service that presents you this information.

When this trust connection is compromised or broken (& the service can not be trusted anymore), it is totally in the hands of the service itself
if they inform the user or not. (e.g. any server hack is only reported to users if the entity that controls the server decides to inform the users of the service).

What BitofTrust is trying to do is to make ALL these trust connections explicit and in total control of the entities involved in those trust relationships.

The fact that an entity trust a piece of information (or not) is an important attribute of that piece of information itself.

## BitofTrust and the current Web

To establish trust the current Web starts from cryptographical privacy of servers and actors.
Servers authenticate against eachother based on symmetric and asymmetric encryption and
actors authenticate against servers based on authentication factors as:

- some physical object in the possession of the user, such as a USB stick with a secret token, a bank card, a key, etc.
- some secret known to the user, such as a password, PIN, TAN, etc.
- some physical characteristic of the user (biometrics), such as a fingerprint, eye iris, voice, typing speed, pattern in key press intervals, etc.
- Somewhere you are, such as connection to a specific computing network or utilizing a GPS signal to identify the location

.. and any combination of the above for higher security levels.

**The current web is about how machines understand trust.**
It starts from a privacy space and a protocol in which a machine or human can proof that they have access to that privacy space.

But this is **not how humans understand trust**. 
Trust for humans is not about the ability to show to somebody else that you have access to a private space.
Trust for humans is allowing the trusted other to become part of your private space.
Trust for humans is about intimacy, not privacy.

> You do not show that you trust someone else by proofing that you have a key to your private house, you show it by allowing the other to enter your private house.

Trust for humans is more about authorisation & permission than about authentication. 
Trust can be given by humans without formal authentication. By sharing your data to the public, you give trust.
Machines only really understand trust after authentication in the current web.

And this difference in how trust is build digital and how it is build between humans is an important
mismatch in the current web because it shows in our day to day usage of the web:

If you surf to a website in the current web, the browser will tell you the domain name that you are on,
it will warn you if the connection is secure and a SSL certificate (https) is present or not.
The browser environment shows you the code that is running at the client. But what it doesn't show, is what is 
running at the server side. A user does not have any control on what is happening on the server in many cases.

And this is where the human trust factor (and also a lot of legal text like *Terms of Service*, *Privacy Policy* and *Explicit Consent*) 
comes in. The trust a human puts in their interaction with a website shows by his/her
willingness to share his private space (information, password, data,...) with the server behind that website.
And this is not only based on a cryptographical security of the channel over which this data is send but based on
factors which are in the current web not captured with the technology itself.
GDPR and other legislation that has been put in place, shows that society is aware of the problematic aspect of this. 
A problem that is now not solved by technology itself but by a legal question for consent added to website before you can start interacting.

Several solutions to this problem have been proposed, many of them promoting the re-decentralisation of the web.
Below we discuss some of these proposed solutions and show how we think their blueprint starting from
privacy & self, actually  is more about accepting (and maybe even promoting) distrust and not about
establishing & enforcing trust.
Current efforts start from guarding your privacy in stead of establishing your intimacy & trust relationships.

Trust is a much more mutual transparent process than current client-server setup of the web. 
In a way you can consider an authentication provider who sends you a message when someone has tried 
to login to your account in a suspicious way as a first step towards a more mutual equal relationship between client & server
but in what follows we will propose a protocol which goes much further than just that.

BitofTrust creates a network that does not start from the privacy of the nodes but from the intimacy of the edges.
And letting machines understand how the persistent & current existence of an edge is the very thing that shows trust.

## BitofTrust and Verifiable Credentials

TBD

## BitofTrust and Solid

TBD

## BitofTrust and Open Badges

TBD
